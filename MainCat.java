public class MainCat {
    public static void main(String[] args) {
        Cat[] cats = {
                new Cat("Кот", "м", 2010),
                new Cat("Муська", "ж", 2013),
                new Cat("Жора", "м", 2009),
                new Cat("Белка", "ж", 2013),
                new Cat("Барсик", "м", 2012)
        };
        int old = 10000;
        for (int i = 0; i < cats.length; i++) {
            if (cats[i].getGender().equals("м") && cats[i].getYear() < old) {
                old = cats[i].getYear();
            }
        }
        System.out.println("Год рождения самого старого кота " + old);
    }
}

